package org.objects;

import javax.persistence.*;
@Entity
@Table(name="package")
public class Package {
	
	
	private int packageId;
	private String sender;
	private String receiver;
	private String name;
	private String description;
	private String senderCity;
	private String destCity;
	private int tracking;
	private String route;
	
	
	public Package(){
		
	/*	this.sender=new Client();
		this.receiver=new Client();
		this.name="";
		this.description="";
		this.senderCity="";
		this.destCity="";
		this.tracking=false;
		this.route=new ArrayList<RouteEntry>();
		*/
	}
	
	
	public Package(String sender,String receiver,String name,String description,
			String senderCity,String destCity,int tracking,String route){
		
		
		this.sender=sender;
		this.receiver=receiver;
		this.name=name;
		this.description=description;
		this.senderCity=senderCity;
		this.destCity=destCity;
		this.tracking=tracking;
		this.route=route;
		
		
	
		
	}

	
	@Id
	@Column(name="packageId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getPackageId() {
		return packageId;
	}

	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}
	

	public String getSender() {
		return sender;
	}


	public void setSender(String sender) {
		this.sender = sender;
	}


	

	public String getReceiver() {
		return receiver;
	}


	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getSenderCity() {
		return senderCity;
	}


	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}


	public String getDestCity() {
		return destCity;
	}


	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}


	public int getTracking() {
		return tracking;
	}


	public void setTracking(int tracking) {
		this.tracking = tracking;
	}


	public String getRoute() {
		return route;
	}


	public void setRoute(String route) {
		this.route = route;
	}
	
	
	
	
	
	
	
	
	
}
