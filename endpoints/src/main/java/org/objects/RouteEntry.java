package org.objects;

import java.util.Date;

public class RouteEntry {
	
	private String city;
	private Date dateTime;
	
	
	public RouteEntry(){
		
		this.city="";
		this.dateTime=null;
		
		
	}

	
	public RouteEntry(String city,Date datetime){
		
		this.city=city;
		this.dateTime=datetime;
		
		
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public Date getDateTime() {
		return dateTime;
	}


	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
	
	
	

}
