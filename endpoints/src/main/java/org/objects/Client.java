package org.objects;

import javax.persistence.*;


@Entity
@Table(name="client")
public class Client {
	
	private String firstName;
	private String lastName;
	private String username;
	private int isAdmin;
	private String password;
	
	
	public Client(){
		
	/*	this.firstName="";
		this.lastName="";
		this.username="";
		this.isAdmin=false;
		this.password="";
		*/
		
		
	}
	
	
	public Client(String firstName,String lastName,
			String username,int isAdmin,String password){
		
		this.firstName=firstName;
		this.lastName=lastName;
		this.username=username;
		this.isAdmin=isAdmin;
		this.password=password;
		
		
		
		
	}

	@Id
	@Column(name="username")
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public int isIsAdmin() {
		return isAdmin;
	}


	public void setIsAdmin(int isAdmin) {
		this.isAdmin = isAdmin;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	

}
