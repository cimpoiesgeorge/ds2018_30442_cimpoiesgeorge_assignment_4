package org.endpoints;


import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.access.ClientManager;
import org.access.PackageManager;
import org.objects.Client;





@WebService(endpointInterface = "org.endpoints.EndpointsInterface")
public class Endpoints implements EndpointsInterface {
	
	public String printMessage(String message) {
		return "Here to print the message:" + message;
	}
	
	public int checkLogIn(String username, String password){
		
		ClientManager clientManager=new ClientManager();
    	clientManager.setup();
    	
    	if(clientManager.read(username).getPassword().equals(password)) return 1;
		
		return 0;
	}
	
	
	public void createAccount(String username,String firstName,String lastName, String password){
		
		ClientManager clientManager=new ClientManager();
    	clientManager.setup();
    	
    	clientManager.create(firstName, lastName, 0, username, password);
		
		
	}
	
	
	
	public String getPackages(){
	
	PackageManager packageManager=new PackageManager();
	packageManager.setup();
	
	String string="NAME PACKAGE_ID DESCRIPTION SENDER_CITY DEST_CITY RECIEVER SENDER TRACKING GET ROUTE \n\n";
	
	for(org.objects.Package p:packageManager.getList()){
		
		
		string+=p.getName()+" "+p.getPackageId()+p.getDescription()+" "+p.getSenderCity()+" "+p.getDestCity()+" "+p.getReceiver()+" "+p.getSender()+" "+p.getTracking()+" "+p.getRoute()+"\n";
	}
	
	return string;
	
}
	
	
	public void createPack(String sender,String reciever,String name,String description,String senderCity,String destCity,int tracking,String route){
		
		PackageManager packageManager=new PackageManager();
		packageManager.setup();
		
		packageManager.create(sender, reciever, name, description, senderCity, destCity, tracking, route);
		
		
	
	}
	
public void updatePack(int packageId,String sender,String reciever,String name,String description,String senderCity,String destCity,int tracking,String route){
		
		PackageManager packageManager=new PackageManager();
		packageManager.setup();
		
		packageManager.update(packageId,sender, reciever, name, description, senderCity, destCity, tracking, route);
		
		
	
	}

public void deletePack(int packageId){
	
	PackageManager packageManager=new PackageManager();
	packageManager.setup();
	
	packageManager.delete(packageId);
	
}
	

public String getPackagesRecieved(String username){
	
	PackageManager packageManager=new PackageManager();
	packageManager.setup();
	
	String string="NAME PACKAGE_ID DESCRIPTION SENDER_CITY DEST_CITY RECIEVER SENDER TRACKING GET ROUTE \n\n";
	
	for(org.objects.Package p:packageManager.getList()){
		
		if (username.equals(p.getReceiver())&&p.getTracking()==1)
		
		string+=p.getName()+" "+p.getPackageId()+p.getDescription()+" "+p.getSenderCity()+" "+p.getDestCity()+" "+p.getReceiver()+" "+p.getSender()+" "+p.getTracking()+" "+p.getRoute()+"\n";
	}
	
	return string;
	
}

public String getPackagesSent(String username){
	
	PackageManager packageManager=new PackageManager();
	packageManager.setup();
	
	String string="NAME PACKAGE_ID DESCRIPTION SENDER_CITY DEST_CITY RECIEVER SENDER TRACKING GET ROUTE \n\n";
	
	for(org.objects.Package p:packageManager.getList()){
		
		if (username.equals(p.getSender())&&p.getTracking()==1)
		string+=p.getName()+" "+p.getPackageId()+p.getDescription()+" "+p.getSenderCity()+" "+p.getDestCity()+" "+p.getReceiver()+" "+p.getSender()+" "+p.getTracking()+" "+p.getRoute()+"\n";
	}
	
	return string;
	
}

public String getPackagesAll(String username){
	
	PackageManager packageManager=new PackageManager();
	packageManager.setup();
	
	String string="NAME PACKAGE_ID DESCRIPTION SENDER_CITY DEST_CITY RECIEVER SENDER TRACKING GET ROUTE \n\n";
	
	for(org.objects.Package p:packageManager.getList()){
		
		if ((username.equals(p.getReceiver())||username.equals(p.getSender()))&&p.getTracking()==1)
		string+=p.getName()+" "+p.getPackageId()+p.getDescription()+" "+p.getSenderCity()+" "+p.getDestCity()+" "+p.getReceiver()+" "+p.getSender()+" "+p.getTracking()+" "+p.getRoute()+"\n";
	}
	
	return string;
	
}

public int checkAdmin(String username){
	
	ClientManager clientManager=new ClientManager();
	clientManager.setup();
	
	if(clientManager.read(username).isIsAdmin()==1) return 1;
	
	return 0;
}









	
	
}

