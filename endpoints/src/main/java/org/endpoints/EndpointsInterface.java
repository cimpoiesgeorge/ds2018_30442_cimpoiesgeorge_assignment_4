package org.endpoints;

import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

@WebService
@SOAPBinding(style = Style.RPC)
public interface EndpointsInterface {

	
	public String printMessage(String message);
	
	public int checkLogIn(String username, String password);
	
	public void createAccount(String username,String firstName,String lastName, String password);
	
	public  String getPackages();

	public void createPack(String sender,String reciever,String name,String description,String senderCity,String destCity,int tracking,String route);
	
	public void updatePack(int packageId,String sender,String reciever,String name,String description,String senderCity,String destCity,int tracking,String route);
	
	public void deletePack(int packageId);
	
	public String getPackagesAll(String username);
	
	public String getPackagesSent(String username);
	
	public String getPackagesRecieved(String username);
	
	public int checkAdmin(String username);
}
