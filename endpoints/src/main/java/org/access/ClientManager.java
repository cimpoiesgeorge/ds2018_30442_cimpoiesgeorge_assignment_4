package org.access;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.objects.Client;
public class ClientManager {
	
protected static SessionFactory sessionFactory;
	
	public static void setup(){
		

		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure("src/main/resources/hibernate.cfg.xml") // configures
																									// settings
																									// from
																									// hibernate.cfg.xml
				.build();
		try {
			sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception ex) {
			System.out.println("I am here client");
			ex.printStackTrace();
			StandardServiceRegistryBuilder.destroy(registry);
		}
	
	}
	
	protected void exit(){
		
		sessionFactory.close();
	}
	
	public static void create(String firstName,String lastName,int isAdmin,String username, String password){
		
		Client client=new Client(firstName,lastName,username,isAdmin,password);
		
		
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(client);
		
		session.getTransaction().commit();
		session.close();

		
	}
	
	
	public static Client read(String username){
		
		
	
		Session session=sessionFactory.openSession();
		
		
		Client client=session.get(Client.class, username);
		
		
		session.close();
		return client;
	}
	
public static void update(String firstName,String lastName,int isAdmin,String username, String password){
		
		Client client=read(username);
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		client.setIsAdmin(isAdmin);
		client.setPassword(password);
		client.setUsername(username);
		client.setFirstName(firstName);
		client.setLastName(lastName);
	
		
		session.update(client);
		session.close();
		
	}


public static void delete(String username){
	
	Session session=sessionFactory.openSession();
	session.beginTransaction();
	
	session.delete(read(username));
	
	session.getTransaction().commit();
	session.close();
}

public static List<Client> getList(){
	
	Session session = sessionFactory.openSession();
	session.beginTransaction();
	
	List<Client> list = session.createCriteria(Client.class).list();
	 
	session.getTransaction().commit();
	  
	 session.close();
	 
	 return list;

}

public static void main(String[] args) {
	System.setProperty("hibernate.dialect.storage_engine", "innodb");
	// code to run the program
	ClientManager manager = new ClientManager();
	manager.setup();

	manager.exit();
}

}
