package org.access;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.objects.Package;




public class PackageManager {
	
protected static SessionFactory sessionFactory;
	
	public void setup(){
		

		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure("src/main/resources/hibernate.cfg.xml") // configures
																									// settings
																									// from
																									// hibernate.cfg.xml
				.build();
		try {
			sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception ex) {
			System.out.println("I am here");
			ex.printStackTrace();
			StandardServiceRegistryBuilder.destroy(registry);
		
		}
	
	}
	
	protected void exit(){
		
		sessionFactory.close();
	}
	
	public static void create(String sender,String reciver,String name,String description,String senderCity,String destCity,int tracking,String route){
		
		Package pac= new Package(sender,reciver,name,description,senderCity,destCity,tracking,route);
		
		
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(pac);
		
		session.getTransaction().commit();
		session.close();

		
	}
	
	
	public static Package read(int packageId){
		
		Session session=sessionFactory.openSession();
		
		
		Package pac=session.get(Package.class, packageId);
		
		
		session.close();
		return pac;
	}
	
public static void update(int packageId,String sender,String receiver,String name,String description,String senderCity,String destCity,int tracking,String route){
		
		Package pac=read(packageId);
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		pac.setDescription(description);
		pac.setDestCity(destCity);
		pac.setName(name);
		pac.setReceiver(receiver);
		pac.setRoute(route);
		pac.setSender(sender);
		pac.setSenderCity(senderCity);
		pac.setTracking(tracking);
	
		
	
		
		session.update(pac);
		session.close();
		
	}


public static void delete(int packageId){
	
	Session session=sessionFactory.openSession();
	session.beginTransaction();
	
	session.delete(read(packageId));
	
	session.getTransaction().commit();
	session.close();
}

public static List<org.objects.Package> getList(){
	
	Session session = sessionFactory.openSession();
	session.beginTransaction();
	
	List<org.objects.Package> list = session.createCriteria(org.objects.Package.class).list();
	 
	session.getTransaction().commit();
	  
	 session.close();
	 
	 return list;

}

public static void main(String[] args) {
	// code to run the program
	System.setProperty("hibernate.dialect.storage_engine", "innodb");
	ClientManager manager = new ClientManager();
	manager.setup();

	manager.exit();
}

}
