/**
 * EndpointsInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.endpoints;

public interface EndpointsInterface extends java.rmi.Remote {
    public java.lang.String getPackages() throws java.rmi.RemoteException;
    public java.lang.String getPackagesRecieved(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String printMessage(java.lang.String arg0) throws java.rmi.RemoteException;
    public int checkLogIn(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void updatePack(int arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, java.lang.String arg6, int arg7, java.lang.String arg8) throws java.rmi.RemoteException;
    public void deletePack(int arg0) throws java.rmi.RemoteException;
    public java.lang.String getPackagesAll(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String getPackagesSent(java.lang.String arg0) throws java.rmi.RemoteException;
    public void createAccount(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public void createPack(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, int arg6, java.lang.String arg7) throws java.rmi.RemoteException;
    public int checkAdmin(java.lang.String arg0) throws java.rmi.RemoteException;
}
