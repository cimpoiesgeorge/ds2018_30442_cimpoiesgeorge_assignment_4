/**
 * EndpointsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.endpoints;

public interface EndpointsService extends javax.xml.rpc.Service {
    public java.lang.String getEndpointsPortAddress();

    public org.endpoints.EndpointsInterface getEndpointsPort() throws javax.xml.rpc.ServiceException;

    public org.endpoints.EndpointsInterface getEndpointsPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
