/**
 * EndpointsServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.endpoints;

public class EndpointsServiceLocator extends org.apache.axis.client.Service implements org.endpoints.EndpointsService {

    public EndpointsServiceLocator() {
    }


    public EndpointsServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public EndpointsServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for EndpointsPort
    private java.lang.String EndpointsPort_address = "http://localhost:8080/tema4/api";

    public java.lang.String getEndpointsPortAddress() {
        return EndpointsPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String EndpointsPortWSDDServiceName = "EndpointsPort";

    public java.lang.String getEndpointsPortWSDDServiceName() {
        return EndpointsPortWSDDServiceName;
    }

    public void setEndpointsPortWSDDServiceName(java.lang.String name) {
        EndpointsPortWSDDServiceName = name;
    }

    public org.endpoints.EndpointsInterface getEndpointsPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(EndpointsPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getEndpointsPort(endpoint);
    }

    public org.endpoints.EndpointsInterface getEndpointsPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.endpoints.EndpointsPortBindingStub _stub = new org.endpoints.EndpointsPortBindingStub(portAddress, this);
            _stub.setPortName(getEndpointsPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setEndpointsPortEndpointAddress(java.lang.String address) {
        EndpointsPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.endpoints.EndpointsInterface.class.isAssignableFrom(serviceEndpointInterface)) {
                org.endpoints.EndpointsPortBindingStub _stub = new org.endpoints.EndpointsPortBindingStub(new java.net.URL(EndpointsPort_address), this);
                _stub.setPortName(getEndpointsPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("EndpointsPort".equals(inputPortName)) {
            return getEndpointsPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://endpoints.org/", "EndpointsService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://endpoints.org/", "EndpointsPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("EndpointsPort".equals(portName)) {
            setEndpointsPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
