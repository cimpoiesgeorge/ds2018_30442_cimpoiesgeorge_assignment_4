package org.endpoints;

public class EndpointsInterfaceProxy implements org.endpoints.EndpointsInterface {
  private String _endpoint = null;
  private org.endpoints.EndpointsInterface endpointsInterface = null;
  
  public EndpointsInterfaceProxy() {
    _initEndpointsInterfaceProxy();
  }
  
  public EndpointsInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initEndpointsInterfaceProxy();
  }
  
  private void _initEndpointsInterfaceProxy() {
    try {
      endpointsInterface = (new org.endpoints.EndpointsServiceLocator()).getEndpointsPort();
      if (endpointsInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)endpointsInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)endpointsInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (endpointsInterface != null)
      ((javax.xml.rpc.Stub)endpointsInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.endpoints.EndpointsInterface getEndpointsInterface() {
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    return endpointsInterface;
  }
  
  public int checkLogIn(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    return endpointsInterface.checkLogIn(arg0, arg1);
  }
  
  public java.lang.String printMessage(java.lang.String arg0) throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    return endpointsInterface.printMessage(arg0);
  }
  
  public void createAccount(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    endpointsInterface.createAccount(arg0, arg1, arg2, arg3);
  }
  
  public java.lang.String getPackages() throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    return endpointsInterface.getPackages();
  }
  
  public void deletePack(int arg0) throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    endpointsInterface.deletePack(arg0);
  }
  
  public void createPack(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, int arg6, java.lang.String arg7) throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    endpointsInterface.createPack(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
  }
  
  public void updatePack(int arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, java.lang.String arg6, int arg7, java.lang.String arg8) throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    endpointsInterface.updatePack(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  }
  
  public java.lang.String getPackagesRecieved(java.lang.String arg0) throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    return endpointsInterface.getPackagesRecieved(arg0);
  }
  
  public java.lang.String getPackagesSent(java.lang.String arg0) throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    return endpointsInterface.getPackagesSent(arg0);
  }
  
  public java.lang.String getPackagesAll(java.lang.String arg0) throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    return endpointsInterface.getPackagesAll(arg0);
  }
  
  public int checkAdmin(java.lang.String arg0) throws java.rmi.RemoteException{
    if (endpointsInterface == null)
      _initEndpointsInterfaceProxy();
    return endpointsInterface.checkAdmin(arg0);
  }
  
  
}