package org.client;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.xml.rpc.ServiceException;

import org.endpoints.EndpointsInterface;
import org.endpoints.EndpointsService;
import org.endpoints.EndpointsServiceLocator;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.awt.event.ActionEvent;

public class LogIn {

	JFrame frame;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogIn window = new LogIn();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LogIn() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
	    	
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblUsername = new JLabel("USERNAME:");
		lblUsername.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		lblUsername.setBounds(47, 83, 113, 14);
		frame.getContentPane().add(lblUsername);
		
		textField = new JTextField();
		textField.setBounds(46, 111, 179, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblPassword = new JLabel("PASSWORD:");
		lblPassword.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		lblPassword.setBounds(48, 155, 113, 14);
		frame.getContentPane().add(lblPassword);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(47, 183, 179, 20);
		frame.getContentPane().add(textField_1);
		
		JButton btnNewButton = new JButton("Log In");
		btnNewButton.setFont(new Font("Segoe WP", Font.PLAIN, 16));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				
				String username=textField.getText();
				String password=textField_1.getText();
				
				
				 EndpointsService serv = new EndpointsServiceLocator();
			    	try {
						EndpointsInterface endpoints = serv.getEndpointsPort();
						if (endpoints.checkLogIn(username,password)==1){
							
							System.out.println("Log In");
							
							if(endpoints.checkAdmin(username)==1)
							
							{
							Admin window = new Admin();
							window.frame.setVisible(true);
							
							frame.dispose();
							} else{
								Client window = new Client();
								window.frame.setVisible(true);
								
								frame.dispose();
								
							}
							
						} else {
							JOptionPane.showMessageDialog(frame, "Please Try Again!", "LogIn Fail", JOptionPane.PLAIN_MESSAGE);
							System.out.println("Log In FAIL");
						}
					} catch (ServiceException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				
				
				
			
				
				
				
				
				
				
			}
		});
		btnNewButton.setBounds(287, 72, 113, 59);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnCenter = new JButton("Register");
		btnCenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				//dispose();
				RegisterWindow window = new RegisterWindow();
				window.frame.setVisible(true);
				
				frame.dispose();
			}
		});
		btnCenter.setFont(new Font("Segoe WP", Font.PLAIN, 16));
		btnCenter.setBounds(287, 144, 113, 59);
		frame.getContentPane().add(btnCenter);
	}
}
