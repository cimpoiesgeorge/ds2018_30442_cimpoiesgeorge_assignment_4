package org.client;

import java.awt.EventQueue;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.xml.rpc.ServiceException;

import org.endpoints.EndpointsInterface;
import org.endpoints.EndpointsService;
import org.endpoints.EndpointsServiceLocator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Client {

	JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Client window = new Client();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Client() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 915, 490);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		final JTextArea textArea = new JTextArea();
		textArea.setBounds(30, 39, 797, 133);
		frame.getContentPane().add(textArea);
		
		
		
		
		  EndpointsService serv = new EndpointsServiceLocator();
	    	try {
				final EndpointsInterface endpoints = serv.getEndpointsPort();
				
				
				textArea.setText(endpoints.getPackages());
				
				JLabel lblId = new JLabel("USER TO SEARCH FROM:");
				lblId.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				lblId.setBounds(41, 193, 207, 14);
				frame.getContentPane().add(lblId);
				
				textField = new JTextField();
				textField.setColumns(10);
				textField.setBounds(40, 221, 179, 20);
				frame.getContentPane().add(textField);
				
				JButton btnCreate = new JButton("RECIEVER");
				btnCreate.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
					//	int id=Integer.parseInt(textField.getText());
						String user=textField.getText();
						
						
					
						
						try {
							textArea.setText(endpoints.getPackagesRecieved(user));
						} catch (RemoteException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						
					}
				});
				btnCreate.setFont(new Font("Segoe WP", Font.PLAIN, 16));
				btnCreate.setBounds(574, 264, 113, 59);
				frame.getContentPane().add(btnCreate);
				
				JButton btnDelete = new JButton("SENDER");
				btnDelete.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						String user=textField.getText();
						
						
						
						try {
							textArea.setText(endpoints.getPackagesSent(user));
						} catch (RemoteException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
				btnDelete.setFont(new Font("Segoe WP", Font.PLAIN, 16));
				btnDelete.setBounds(574, 336, 113, 59);
				frame.getContentPane().add(btnDelete);
				
				JButton btnLogOut = new JButton("Log Out");
				btnLogOut.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						
						
						
						
						LogIn window = new LogIn();
						window.frame.setVisible(true);
						
						frame.dispose();
					}
				});
				btnLogOut.setFont(new Font("Segoe WP", Font.PLAIN, 16));
				btnLogOut.setBounds(714, 264, 113, 59);
				frame.getContentPane().add(btnLogOut);
				
				JButton btnUpdate = new JButton("ALL");
				btnUpdate.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						
							
							String user=textField.getText();
						
							
							
							try {
								textArea.setText(endpoints.getPackagesAll(user));
							} catch (RemoteException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
					}
				});
				btnUpdate.setFont(new Font("Segoe WP", Font.PLAIN, 16));
				btnUpdate.setBounds(714, 336, 113, 59);
				frame.getContentPane().add(btnUpdate);
				 
				
				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    }
	}

