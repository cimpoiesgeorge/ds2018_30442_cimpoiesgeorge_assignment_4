package org.client;

import java.awt.EventQueue;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.xml.rpc.ServiceException;

import org.endpoints.EndpointsInterface;
import org.endpoints.EndpointsService;
import org.endpoints.EndpointsServiceLocator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Admin {

	JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Admin window = new Admin();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Admin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 915, 490);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		final JTextArea textArea = new JTextArea();
		textArea.setBounds(30, 39, 797, 133);
		frame.getContentPane().add(textArea);
		
		
		
		
		  EndpointsService serv = new EndpointsServiceLocator();
	    	try {
				final EndpointsInterface endpoints = serv.getEndpointsPort();
				
				
				textArea.setText(endpoints.getPackages());
				
				JLabel lblId = new JLabel("ID:");
				lblId.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				lblId.setBounds(41, 193, 113, 14);
				frame.getContentPane().add(lblId);
				
				textField = new JTextField();
				textField.setColumns(10);
				textField.setBounds(40, 221, 179, 20);
				frame.getContentPane().add(textField);
				
				JLabel lblSender = new JLabel("SENDER:");
				lblSender.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				lblSender.setBounds(41, 252, 113, 14);
				frame.getContentPane().add(lblSender);
				
				textField_1 = new JTextField();
				textField_1.setColumns(10);
				textField_1.setBounds(40, 280, 179, 20);
				frame.getContentPane().add(textField_1);
				
				JLabel lblReciver = new JLabel("RECIEVER:");
				lblReciver.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				lblReciver.setBounds(41, 311, 113, 14);
				frame.getContentPane().add(lblReciver);
				
				textField_2 = new JTextField();
				textField_2.setColumns(10);
				textField_2.setBounds(40, 339, 179, 20);
				frame.getContentPane().add(textField_2);
				
				JLabel lblName = new JLabel("NAME:");
				lblName.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				lblName.setBounds(41, 371, 113, 14);
				frame.getContentPane().add(lblName);
				
				textField_3 = new JTextField();
				textField_3.setColumns(10);
				textField_3.setBounds(40, 399, 179, 20);
				frame.getContentPane().add(textField_3);
				
				JLabel lblDescription = new JLabel("DESCRIPTION:");
				lblDescription.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				lblDescription.setBounds(308, 193, 113, 14);
				frame.getContentPane().add(lblDescription);
				
				textField_4 = new JTextField();
				textField_4.setColumns(10);
				textField_4.setBounds(307, 221, 179, 20);
				frame.getContentPane().add(textField_4);
				
				JLabel lblSenderCity = new JLabel("SENDER CITY:");
				lblSenderCity.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				lblSenderCity.setBounds(308, 252, 113, 14);
				frame.getContentPane().add(lblSenderCity);
				
				textField_5 = new JTextField();
				textField_5.setColumns(10);
				textField_5.setBounds(307, 280, 179, 20);
				frame.getContentPane().add(textField_5);
				
				JLabel lblRecieverCity = new JLabel("RECIEVER CITY:");
				lblRecieverCity.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				lblRecieverCity.setBounds(308, 311, 113, 14);
				frame.getContentPane().add(lblRecieverCity);
				
				textField_6 = new JTextField();
				textField_6.setColumns(10);
				textField_6.setBounds(307, 339, 179, 20);
				frame.getContentPane().add(textField_6);
				
				JLabel lblTracking = new JLabel("TRACKING:");
				lblTracking.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				lblTracking.setBounds(308, 371, 113, 14);
				frame.getContentPane().add(lblTracking);
				
				textField_7 = new JTextField();
				textField_7.setColumns(10);
				textField_7.setBounds(307, 399, 179, 20);
				frame.getContentPane().add(textField_7);
				
				JLabel lblRoute = new JLabel("ROUTE:");
				lblRoute.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				lblRoute.setBounds(509, 193, 113, 14);
				frame.getContentPane().add(lblRoute);
				
				textField_8 = new JTextField();
				textField_8.setColumns(10);
				textField_8.setBounds(508, 221, 179, 20);
				frame.getContentPane().add(textField_8);
				
				JButton btnCreate = new JButton("Create");
				btnCreate.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
					//	int id=Integer.parseInt(textField.getText());
						String sender=textField_1.getText();
						String reciever=textField_2.getText();
						String name=textField_3.getText();
						String description=textField_4.getText();
						String senderCity=textField_5.getText();
						String recieverCity=textField_6.getText();
						int tracking=Integer.parseInt(textField_7.getText());
						String route=textField_8.getText();
						
						
						
						
						try {
							endpoints.createPack(sender, reciever, name, description, senderCity, recieverCity, tracking,route);
						} catch (RemoteException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						try {
							textArea.setText(endpoints.getPackages());
						} catch (RemoteException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						
					}
				});
				btnCreate.setFont(new Font("Segoe WP", Font.PLAIN, 16));
				btnCreate.setBounds(574, 264, 113, 59);
				frame.getContentPane().add(btnCreate);
				
				JButton btnDelete = new JButton("Delete");
				btnDelete.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						int id=Integer.parseInt(textField.getText());
						try {
							endpoints.deletePack(id);
							textArea.setText(endpoints.getPackages());
						} catch (RemoteException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
				btnDelete.setFont(new Font("Segoe WP", Font.PLAIN, 16));
				btnDelete.setBounds(574, 336, 113, 59);
				frame.getContentPane().add(btnDelete);
				
				JButton btnLogOut = new JButton("Log Out");
				btnLogOut.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						
						
						
						
						LogIn window = new LogIn();
						window.frame.setVisible(true);
						
						frame.dispose();
					}
				});
				btnLogOut.setFont(new Font("Segoe WP", Font.PLAIN, 16));
				btnLogOut.setBounds(714, 264, 113, 59);
				frame.getContentPane().add(btnLogOut);
				
				JButton btnUpdate = new JButton("Update");
				btnUpdate.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						
							int id=Integer.parseInt(textField.getText());
							String sender=textField_1.getText();
							String reciever=textField_2.getText();
							String name=textField_3.getText();
							String description=textField_4.getText();
							String senderCity=textField_5.getText();
							String recieverCity=textField_6.getText();
							int tracking=Integer.parseInt(textField_7.getText());
							String route=textField_8.getText();
							
							
							try {
								endpoints.updatePack(id,sender, reciever, name, description, senderCity, recieverCity, tracking,route);
							} catch (RemoteException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							try {
								textArea.setText(endpoints.getPackages());
							} catch (RemoteException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
					}
				});
				btnUpdate.setFont(new Font("Segoe WP", Font.PLAIN, 16));
				btnUpdate.setBounds(714, 336, 113, 59);
				frame.getContentPane().add(btnUpdate);
				 
				
				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    }
	}

