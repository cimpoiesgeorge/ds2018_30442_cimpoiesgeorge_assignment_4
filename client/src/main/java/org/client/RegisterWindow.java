package org.client;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.rpc.ServiceException;

import org.endpoints.EndpointsInterface;
import org.endpoints.EndpointsService;
import org.endpoints.EndpointsServiceLocator;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.awt.event.ActionEvent;

public class RegisterWindow {

	JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterWindow window = new RegisterWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RegisterWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 329);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.setBounds(10, 11, 434, 261);
		frame.getContentPane().add(panel);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(26, 52, 179, 20);
		panel.add(textField);
		
		JLabel label = new JLabel("FIRSTNAME:");
		label.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		label.setBounds(27, 24, 113, 14);
		panel.add(label);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(26, 111, 179, 20);
		panel.add(textField_1);
		
		JLabel label_1 = new JLabel("LASTNAME:");
		label_1.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		label_1.setBounds(27, 83, 113, 14);
		panel.add(label_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(26, 170, 179, 20);
		panel.add(textField_2);
		
		JLabel label_2 = new JLabel("USER:");
		label_2.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		label_2.setBounds(27, 142, 113, 14);
		panel.add(label_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(26, 230, 179, 20);
		panel.add(textField_3);
		
		JLabel label_3 = new JLabel("PASSWORD:");
		label_3.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		label_3.setBounds(27, 202, 113, 14);
		panel.add(label_3);
		
		JButton button = new JButton("Register");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String firstName=textField.getText();
				String lastName=textField_1.getText();
				String user=textField_2.getText();
				String pass=textField_3.getText();
				
				
				 EndpointsService serv = new EndpointsServiceLocator();
				try {
					EndpointsInterface endpoints = serv.getEndpointsPort();
					JFrame framee = new JFrame("SUCCESS");
					endpoints.createAccount(user,firstName, lastName, pass);
					JOptionPane.showMessageDialog(framee, "Registered!", "Success", JOptionPane.PLAIN_MESSAGE);
					LogIn window = new LogIn();
					window.frame.setVisible(true);
					
						frame.dispose();
				} catch (ServiceException e5) {
					// TODO Auto-generated catch block
					e5.printStackTrace();
				} catch (RemoteException e4) {
					// TODO Auto-generated catch block
					e4.printStackTrace();
				}
			
				
				
			}
		});
		button.setFont(new Font("Segoe WP", Font.PLAIN, 16));
		button.setBounds(295, 38, 113, 59);
		panel.add(button);
	}
}
