package org.client;

import java.rmi.RemoteException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.rpc.ServiceException;


import org.endpoints.EndpointsInterface;
import org.endpoints.EndpointsService;
import org.endpoints.EndpointsServiceLocator;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        EndpointsService serv = new EndpointsServiceLocator();
    	try {
			EndpointsInterface endpoints = serv.getEndpointsPort();
			String s = endpoints.printMessage(" Hello, George!");
			JFrame frame = new JFrame("Homework 4");
			JOptionPane.showMessageDialog(frame, s, "Success", JOptionPane.PLAIN_MESSAGE);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}
